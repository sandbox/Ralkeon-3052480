<?php

namespace Drupal\icon_selector\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class IconSelectorConfigForm.
 */
class IconSelectorConfigForm extends ConfigFormBase {

  /** @var string Config settings */
  const SETTINGS = 'icon_selector.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'icon_selector_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('icon_selector.settings');

    $form['icons_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Icons path'),
      '#description' => $this->t('Insert the local path to your custom icons folder (Default path: public://icon_selector)'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('icons_path') ? $config->get('icons_path') : 'public://icon_selector',
      '#weight' => '0',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $key => $value) {
      $config = \Drupal::service('config.factory')->getEditable('icon_selector.settings');

      $config->set($key, $value)->save();
      \Drupal::logger('icon_selector')->notice($key . ': ' . $value);
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

}
