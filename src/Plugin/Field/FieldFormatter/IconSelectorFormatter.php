<?php

namespace Drupal\icon_selector\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'iconselector' formatter.
 *
 * @FieldFormatter(
 *   id = "iconselector_formatter",
 *   module = "icon_selector",
 *   label = @Translation("Icon Selector"),
 *   field_types = {
 *     "iconselector"
 *   }
 * )
 */
class IconSelectorFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements   = [];
    $icons_path = drupal_get_path('module', 'icon_selector') . '/icons';
    $tree       = $this->recursiveDirScan($icons_path);
    $this->getIcon($items, $icons_path, $tree, $elements);
    return $elements;
  }

  /**
   * Get the icon to print.
   */
  public function getIcon($items, $icons_path, $tree, &$elements) {
    if (is_array($tree)) {
      foreach ($items as $delta => $item) {
        if (isset($tree[$item->value])) {
          $icon_url         = $tree[$item->value]['file_url'];
          $icon_content     = file_get_contents($icon_url);
          $elements[$delta] = [
            '#theme'        => 'icon_selector_element',
            '#icon_content' => $icon_content,
            '#attributes'   => [
              'id'    => 'icon-selector',
              'class' => [
                'icon-selector-element',
              ],
            ],
          ];
          return $elements;
        }
      }
    }

    foreach ($tree as $tree_item) {
      if (is_array($tree_item) and !isset($tree_item["filename"])) {
        return $this->getIcon($items, $icons_path, $tree_item, $elements);
      }
    }
  }

  /**
   * Get directory tree.
   */
  public function recursiveDirScan($path, $tree = []) {
    global $base_url;
    foreach (scandir($path) as $filename) {
      if ($filename[0] === '.') {
        continue;
      }

      $filePath = $path . '/' . $filename;
      if (is_dir($filePath)) {
        if (!isset($tree[$filename])) {
          $tree[$filename] = [];
        }

        $tree[$filename] = $this->recursiveDirScan($filePath, $tree[$filename]);
      }
      else {
        $tree[str_replace('.svg', '', $filename)] = [
          'filename' => $filename,
          'file_url' => $base_url . '/' . str_replace(DRUPAL_ROOT, '', $filePath),
        ];
      }
    }

    return $tree;
  }

}
