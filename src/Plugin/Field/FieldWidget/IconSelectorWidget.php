<?php

namespace Drupal\icon_selector\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'Iconselector' widget.
 *
 * @FieldWidget(
 *   id = "iconselector_widget",
 *   module = "icon_selector",
 *   label = @Translation("Icon Selector"),
 *   field_types = {
 *     "iconselector"
 *   }
 * )
 */
class IconSelectorWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $config = \Drupal::config('icon_selector.settings');

    $default_icons_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'icon_selector') . '/icons';
    $default_icons      = $this->getIcons($default_icons_path);

    $config_icons = [];
    if ($config->get('icons_path') !== NULL) {
      $config_icons_path_raw = \Drupal::service('file_system')
        ->realpath($config->get('icons_path'));
      $config_icons_path     = $config_icons_path_raw;
      $config_icons          = $this->getIcons($config_icons_path);
    }

    $options = array_merge($default_icons, $config_icons);
    if (!empty($options)) {
      $icons       = [];
      $icons_files = [];
      foreach ($options as $key => $value) {
        $this->sortOptionsArray($options, $key, $icons, $icons_files);
      }

      $processed_options = [
        'icons'       => $icons,
        'icons_files' => $icons_files,
      ];

      $element['value'] = $element + [
          '#type'          => 'select',
          '#options'       => $processed_options['icons'],
          '#empty_value'   => '',
          '#default_value' => (isset($items[$delta]->value) && isset($processed_options['icons'][$items[$delta]->value])) ? $items[$delta]->value : NULL,
          '#description'   => t('Select an icon'),
          '#attached'      => [
            'library' => [
              'icon_selector/chosen',
              'icon_selector/icon-selector',
            ],
          ],
        ];

      $element['icon-selector-widget'] = [
        '#theme'      => 'icon_selector_widget',
        '#icons'      => $processed_options['icons_files'],
        '#attributes' => [
          'id'    => 'icon-selector-widget-wrapper',
          'class' => [
            'icon-selector-widget-wrapper',
          ],
        ],
      ];
    }

    return $element;
  }

  public function getIcons($path) {
    $options = $this->recursiveDirScan($path);

    if (isset($options['icons'])) {
      $options['Other']['icons']       = $options['icons'];
      $options['Other']['icons_files'] = $options['icons_files'];
      unset($options['icons']);
      unset($options['icons_files']);
    }

    return $options;
  }

  public function recursiveDirScan($path, $tree = []) {
    global $base_url;
    foreach (scandir($path) as $filename) {
      if ($filename[0] === '.') {
        continue;
      }

      $filePath = $path . '/' . $filename;
      if (is_dir($filePath)) {
        if (!isset($tree[$filename])) {
          $tree[$filename] = [];
        }

        $tree[$filename] = $this->recursiveDirScan($filePath, $tree[$filename]);
      }
      else {
        $tree['icons'][str_replace('.svg', '', $filename)]       = $filename;
        $tree['icons_files'][str_replace('.svg', '', $filename)] = $base_url . str_replace(DRUPAL_ROOT, '', $filePath);
      }
    }

    return $tree;
  }

  public function sortOptionsArray($options, $key, &$icons, &$icons_files) {
    if (isset($options[$key]['icons'])) {
      asort($options[$key]['icons']);
      $icons[$key] = $options[$key]['icons'];
      asort($icons[$key]);
    }

    if (isset($options[$key]['icons_files'])) {
      asort($options[$key]['icons_files']);
      $icons_files[$key] = $options[$key]['icons_files'];
      asort($icons_files[$key]);
    }

    foreach ($options[$key] as $new_key => $value) {
      if ($new_key !== 'icons' and $new_key !== 'icons_files') {
        if (!isset($icons[$key])) {
          $icons[$key] = [];
        }

        if (!isset($icons_files[$key])) {
          $icons_files[$key] = [];
        }

        $this->sortOptionsArray($options[$key], $new_key, $icons[$key], $icons_files[$key]);
      }
    }
  }

}
